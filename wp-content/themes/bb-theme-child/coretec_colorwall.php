<?php

/*
Template Name: Coretec Colorwall Landing Page 

*/
 get_header(); 
 ?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
			<?php
			echo do_shortcode('[fl_builder_insert_layout slug="colorwall-final"]');
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>